# Virtual Reality Content Management System

## Who are these templates for?

Marketers are experts at using a CMS for 2D websites. They are familiar with uploading media and configuring content. I wanted to give them the tools to create immersive 3D experiences uses these same familiar methods.

360 images are easy to create on a phone and 360 video camera are now affordable. I predict marketers have these assets but don't have the tools available to easily distribute them.

## What is the output?

Each template creates a 3D immersive experience with a normal customisable URL.

When viewed on:

*  a virtual reality headset they can be walked around
*  a mobile phone or tablet it acts as a "magic window" into the experience
*  a desktop computer they can be navigated with wasd keys and a moust

## 360 Photos:

Process:

*  Simply open the template
*  Upload the image using the familiar photo uploader. Resize image the same way they have been doing for years.
*  Rotate the image to pick the initial view

CMS:

![360 image cms options](./examples/360-image/360-image-cms.png)

Website output:

![360 image website](./examples/360-image/360-image-output.gif)

## 360 Video:

Process:

*  Simply open the template
*  Upload the video using the familiar video uploader. The video is hosted on the CMS.

CMS:

![360 video cms options](./examples/360-video/360-video-cms.png)

Website output:

![360 video website](./examples/360-video/360-video-output.gif)

## 3D Models:

Unfortunately the HubSpot CMS doesn't currently support the ability to upload models. Instead the model is hosted elsewhere and the URL is supplied.

Process:

*  Simply open the template
*  Enter the URL of the model
*  Change the model's scale
*  Select the background; either a solid colour, 360 image or 360 video

CMS:

![3d-model cms options](./examples/3d-model/3d-model-cms.png)

Website output:

![3d-model website](./examples/3d-model/3d-model-output.gif)

## Animations and Prototyping 3D experiences:

Process:

*  Simply open the template
*  Add an object to the list. This is the same list feature that marketers are familiar with from conventional websites.
*  Add the position and rotation animation keyframes
*  Model the object using primatives or import a custom model

CMS Objects:

![animation objects cms options](./examples/animation/animation-objects-cms.png)

CMS Edit an Object:

![animation edit object cms options](./examples/animation/animation-edit-object-cms.png)

CMS Edit a Keyframe:

![animation edit keyframe cms options](./examples/animation/animation-edit-keyframe-cms.png)

Website output:

![animation website](./examples/animation/animation-output.gif)
